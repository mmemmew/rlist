;;; rlist.el --- Removing registers from list  -*- lexical-binding: t; -*-

;; Author: JSDurand <mmemmew@gmail.com>
;; URL: https://gitlab.com/mmemmew/rlist
;; Keywords: convenience
;; Version: 0.0.1
;; Package-Requires: ((emacs "27.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file adds the facility to remove registers via a Dired-like
;; interface.

;; It is recommended to bind the function `rlist-list-registers' to
;; the key originally bound to `list-registers', if any. Personally I
;; bind it to "C-x v L".

;;; Code:

(require 'register)
(require 'font-lock)

(defgroup rlist ()
  "Interactively remove registers from the list."
  :group 'register)

(defcustom rlist-expert nil
  "Whether deleting registers requires confirmation."
  :risky t
  :group 'rlist
  :type 'boolean)

(defcustom rlist-verbose nil
  "Whether registers should display their full value."
  :group 'rlist
  :type 'boolean)

(defcustom rlist-buffer-name "*rlist*"
  "Name of buffer showing the list of registers."
  :group 'rlist
  :type 'string)

(defface rlist-mark-delete-indicator
  '((((class color) (min-colors 88) (background light))
     :background "#ffcccc" :foreground "#780000")
    (((class color) (min-colors 88) (background dark))
     :background "#77002a" :foreground "#ffb9ab")
    (t :foreground "red"))
  "Face to display registers deletion marks.")

(defface rlist-description '((t :background unspecified))
  "Face to display register description.")

(defface rlist-key
  '((default :inherit (bold rlist-description))
    (((class color) (min-colors 88) (background light))
     :foreground "#0031a9")
    (((class color) (min-colors 88) (background dark))
     :foreground "#2fafff")
    (t :foreground "blue"))
  "Face to display register keys.")

(defface rlist-type
  '((default :inherit (bold rlist-description))
    (((class color) (min-colors 88) (background light))
     :foreground "#8f0075")
    (((class color) (min-colors 88) (background dark))
     :foreground "#f78fe7")
    (t :foreground "magenta"))
  "Face to display register type.")

(defvar rlist-register-regexp (rx-to-string `(seq "Register " (group (1+ (not space))) space))
  "The regular expression to match a register line.")

;; The main entry point of this package.
;;;###autoload
(defun rlist-list-registers (&rest _args)
  "Display a list of nonempty registers saying briefly what they contain.
Additionally this selects the output buffer and puts that buffer
into `rlist-mode'.

The ARGS is there so as to accept arguments in order for it to be
used as a `revert-buffer-function'."
  (interactive)
  (let ((list (copy-sequence register-alist)))
    (setq list (sort list (lambda (a b) (< (car a) (car b)))))
    (with-output-to-temp-buffer rlist-buffer-name
      (dolist (elt list)
	(when (get-register (car elt))
	  (describe-register-1 (car elt) rlist-verbose)
	  (terpri))))
    ;; Only the following two lines differ from `list-registers'.
    (select-window (get-buffer-window rlist-buffer-name))
    (rlist-mode)))

;;; Toggle verbosity

;;;###autoload
(defun rlist-toggle-verbosity ()
  "Toggle `rlist-verbose', which see."
  (interactive)
  (setq rlist-verbose (not rlist-verbose)))

;;; NOTE:

;; The user can customize the variable `font-lock-maximum-decoration'
;; to control how much fontification is desired.

;; Highlight flagged registers.

(defun rlist-highlight-marked-registers (end)
  "Highlight the registers that are marked for deletion from the \
point until END."
  (catch 'matcher
    (forward-line 0)
    (cond
     ((< (next-single-property-change (point) 'marked nil end) end)
      (goto-char (next-single-property-change (point) 'marked nil end))
      (cond
       ((not (get-text-property (point) 'marked))
        (forward-char -1)))
      (or (re-search-forward (concat "\\("
                                     rlist-register-regexp
                                     "\\(:?.\\|\n\\)+?\\)"
                                     rlist-register-regexp)
                             nil t)
          (re-search-forward (concat "\\("
                                     rlist-register-regexp
                                     "\\(:?.\\|\n\\)+\\)")
                             nil t)
          (goto-char end))
      (throw 'matcher t))
     ((rlist-forward-register 1)))))

(defconst rlist-font-lock-keywords
  (cons (list 'rlist-highlight-marked-registers
              '(1 'rlist-mark-delete-indicator t t))
        nil)
  "The default level of font lock keywords for the list of \
registers.")

(defconst rlist-font-lock-keywords-1
  (list
   (list 'rlist-highlight-marked-registers
         '(1 'rlist-mark-delete-indicator t t))
   (list
    (rx-to-string '(seq bol "Register "
                    (group (1+ (not space))) " contains"
                    (*? not-newline)
                    (group
                     (or (1+ digit)
                      (or "text" "file" "file-query" "frameset" "window configuration"
                       "keyboard macro" "rectangle" "buffer position"
                       "marker in no buffer" "buffer" "bookmark")))
                    (* not-newline) eol))
    '(0 'rlist-description)
    '(1 'rlist-key t t)
    '(2 'rlist-type t t)))
  "The highest level of font lock keywords for the list of \
registers.")

;;;###autoload
(define-derived-mode rlist-mode special-mode "RList"
  "Major mode for displaying the list of registers.
\\<rlist-mode-map>Press \\[rlist-delete-registers] to delete a register.
Press \\[rlist-mark-register-forward] to mark a register. And press \
\\[rlist-do-marked-deletion] to delete marked registers.
Press \\[rlist-unmark-register-forward] or \\[rlist-unmark-register-backward] \
to unmark a register.
Press \\[rlist-unmark-all] to unmark all registers.
\\[rlist-forward-register] and \\[rlist-backward-register] allow easy navigations.

\\{rlist-mode-map}"
  (setq-local revert-buffer-function #'rlist-list-registers)
  (setq font-lock-defaults
        '((rlist-font-lock-keywords rlist-font-lock-keywords-1) t nil nil
          (font-lock-mark-block-function . rlist-put-mark-at-register))))

;; (define-key rlist-mode-map (vector ?d) #'rlist-delete-registers)
;; (define-key rlist-mode-map (vector ?m) #'rlist-mark-register-forward)
;; Just for fun.
(setq rlist-mode-map (list 'keymap
                           (cons ?x #'rlist-do-marked-deletion)
                           (cons ?D #'rlist-delete-registers)
                           (cons ?d #'rlist-mark-register-forward)
                           (cons ?t #'rlist-toggle-marks)
                           (cons ?T #'rlist-toggle-verbosity)
                           (cons ?u #'rlist-unmark-register-forward)
                           (cons ?\d #'rlist-unmark-register-backward)
                           (cons ?U #'rlist-unmark-all)
                           (cons ?l #'recenter-top-bottom)
                           (cons ?n #'rlist-forward-register)
                           (cons ?p #'rlist-backward-register)
                           (cons 'return #'rlist-jump-to-register)
                           (cons ?\C-m #'rlist-jump-to-register)))

(defun rlist-internal-delete-registers (registers)
  "The internal function to delete REGISTERS.
This is not inteded to be used interactively, but rather as the
\"universal delete function\", through which other functions
delete registers they want to delete."
  (cond
   ((or rlist-expert
        (y-or-n-p (format "Really delete registers %s? "
                          (mapcar #'string registers))))
    (setq register-alist
          (rlist-delete-from-alist register-alist registers))
    (revert-buffer))))

;;;###autoload
(defun rlist-do-marked-deletion (&optional nomessage)
  "In RList, delete marked registers.
If NOMESSAGE is non-nil, no message is displayed if there are no
marked registers."
  (interactive "P")
  (let ((list-of-registers-to-delete (rlist-get-marked-registers)))
    (cond
     ((consp list-of-registers-to-delete)
      (rlist-internal-delete-registers list-of-registers-to-delete))
     (nomessage)
     ((message "(No deletions requested)")))))

;;;###autoload
(defun rlist-delete-registers (&optional arg)
  "Delete registers.
If there are marked registers, delete those marked registers.
If ARG is omitted or nil, this deletes the register at point.
If ARG is a positive integer, this deletes the next ARG registers.
If ARG is a negative integer, this deletes the previous ARG registers.
If ARG is zero, and if the region is active, then this deletes
the registers in the region.

In any other case, this reads a register from the minibuffer
using `register-read-with-preview'.  This includes the cases when
the ARG is not a number, or when ARG is zero but the region is
not active."
  (interactive "P")
  (let* ((list-of-registers-to-delete
          (or (rlist-get-marked-registers)
              (rlist-read-register-for-mark arg))))
    (rlist-internal-delete-registers list-of-registers-to-delete)))

(defun rlist-delete-from-alist (ls elements)
  "Remove ELEMENTS from LS.
Elements of LS whose `car' equal an element of ELEMENTS are
removed.

This does not modify LS or ELEMENTS.  It returns a copy of LS
with ELEMENTS removed."
  ;; We want to loop from the end so that deletions won't interfere
  ;; with each other, but we cannot access the last element at
  ;; constant time, so we first loop through the list, and then
  ;; collect the indices we want to remove, and then we loop through
  ;; those indices from the end.

  ;; In our case, since both LS and ELEMENTS are sorted, we might have
  ;; a faster implementation which employs the sorted-ness of the
  ;; arguments, but I think it is pre-mature optimisation anyway.
  (let* ((temp (copy-tree ls))
         (counter 0)
         indices-list head)
    (while (consp temp)
      (setq head (car temp))
      (cond
       ((not (consp head)))
       ((member (car head) elements)
        (setq indices-list (cons counter indices-list))))
      (setq temp (cdr temp))
      (setq counter (1+ counter)))
    (setq temp ls)
    ;; Now INDICES-LIST is sorted from the largest to the smallest,
    ;; precisely the order in which we want to loop through.

    ;; NOTE: Using `mapc' is faster than a while loop, as the manual
    ;; says. Since `dolist' is in essence a while loop, using `mapc'
    ;; will be faster. Of course for our purposes this is premature
    ;; optimisation.
    (mapc
     (lambda (index)
       (cond
        ((> index 0)
         ;; Using `setcdr' is more efficient but destructively
         ;; modifies the list. So we used `copy-tree' to prevent the
         ;; destructions.
         (setcdr (nthcdr (1- index) temp)
                 (nthcdr (1+ index) temp)))
        ((setq temp (cdr temp)))))
     indices-list)
    temp))

(defun rlist-get-marked-registers ()
  "Return the list of marked registers."
  (save-excursion
    (goto-char (point-min))
    (let ((regex rlist-register-regexp) result)
      (while (not (eobp))
        (cond ((and (looking-at regex) (get-text-property (point) 'marked))
               (setq result (cons (aref (kbd (match-string-no-properties 1)) 0)
                                  result))))
        (rlist-forward-register 1))
      (nreverse result))))

;;;###autoload
(defun rlist-mark-register (&optional arg)
  "Mark a register.
If ARG is omitted or nil, this marks the register at point.
If ARG is a positive integer, this marks the next ARG registers.
If ARG is a negative integer, this marks the previous ARG registers.
If ARG is zero, and if the region is active, then this marks the
registers in the region.

In any other case, this reads a register from the minibuffer
using `register-read-with-preview'.  This includes the cases when
the ARG is not a number, or when ARG is zero but the region is
not active."
  (interactive "P")
  (let ((inhibit-read-only t)
        (inhibit-modification-hooks t)
        (registers (rlist-read-register-for-mark arg))
        (regex rlist-register-regexp)
        current)
    (save-excursion
      (goto-char (point-min))
      (while (not (eobp))
        (cond
         ((looking-at regex)
          (setq current (aref (kbd (match-string-no-properties 1)) 0))))
        (cond
         ((member current registers)
          (put-text-property (point) (1+ (point)) 'marked t)
          (put-text-property (point) (1+ (point))
                             'display
                             (concat "D " (buffer-substring (point) (1+ (point)))))
          (font-lock-fontify-block)))
        (rlist-forward-register 1)))))

;;;###autoload
(defun rlist-put-mark-at-register ()
  "Put the mark and point around the register at point.
Notice here we are not marking the register for deletion; we
simply put the mark and the point around the register, in terms
of the Emacs parlance. This is similar to the function
`mark-paragraph'."
  (interactive)
  (cond
   ((looking-at-p rlist-register-regexp)
    (push-mark (point))
    (rlist-forward-register 1)
    (forward-char -1)
    (exchange-point-and-mark))
   (t
    (rlist-backward-register 1)
    (push-mark (point))
    (rlist-forward-register 1)
    (forward-char -1)
    (exchange-point-and-mark))))

;;;###autoload
(defun rlist-toggle-marks ()
  "Toggle marks of registers.
Marked registers become unmarked and originally unmarked
registers become marked."
  (interactive)
  (let ((inhibit-read-only t)
        (inhibit-modification-hooks t)
        (regex rlist-register-regexp)
        current)
    (save-excursion
      (goto-char (point-min))
      (while (not (eobp))
        (cond
         ((looking-at regex)
          (setq current (aref (kbd (match-string-no-properties 1)) 0))
          (cond
           ((get-text-property (point) 'marked)
            (remove-text-properties (point) (+ 3 (point)) '(marked))
            (remove-text-properties (point) (+ 3 (point)) '(display)))
           (t
            (put-text-property (point) (1+ (point)) 'marked t)
            (put-text-property (point) (1+ (point))
                               'display
                               (concat "D " (buffer-substring (point) (1+ (point)))))))
          (font-lock-fontify-block)))
        (rlist-forward-register 1)))))

;;;###autoload
(defun rlist-mark-register-forward (&optional arg)
  "Mark ARG registers and advance forward."
  (interactive "p")
  (let ((arg (prefix-numeric-value arg)))
    (rlist-mark-register arg)
    (rlist-forward-register arg)))

;;;###autoload
(defun rlist-mark-register-backward (&optional arg)
  "Mark ARG registers and advance backward."
  (interactive "p")
  (let ((arg (prefix-numeric-value arg)))
    (rlist-mark-register-forward (- arg))))

;;;###autoload
(defun rlist-unmark-register (&optional arg)
  "Unmark a register.
If ARG is omitted or nil, this marks the register at point.
If ARG is a positive integer, this marks the next ARG registers.
If ARG is a negative integer, this marks the previous ARG registers.
If ARG is zero, and if the region is active, then this marks the
registers in the region.

In any other case, this reads a register from the minibuffer
using `register-read-with-preview'.  This includes the cases when
the ARG is not a number, or when ARG is zero but the region is
not active."
  (interactive "P")
  (let ((inhibit-read-only t)
        (inhibit-modification-hooks t)
        (registers (rlist-read-register-for-mark arg))
        (regex rlist-register-regexp)
        current)
    (save-excursion
      (goto-char (point-min))
      (while (not (eobp))
        (cond
         ((looking-at regex)
          (setq current (aref (kbd (match-string-no-properties 1)) 0))))
        (cond
         ((member current registers)
          (remove-text-properties (point) (+ 3 (point)) '(display))
          (remove-text-properties (point) (+ 3 (point)) '(marked))
          (font-lock-fontify-block)))
        (rlist-forward-register 1)))))

;;;###autoload
(defun rlist-unmark-register-forward (&optional arg)
  "Unmark ARG registers and advance forward."
  (interactive "p")
  (let ((arg (prefix-numeric-value arg)))
    (rlist-unmark-register arg)
    (rlist-forward-register arg)))

;;;###autoload
(defun rlist-unmark-register-backward (&optional arg)
  "Unmark ARG registers and advance backward."
  (interactive "p")
  (rlist-unmark-register-forward (- (prefix-numeric-value arg))))

;;;###autoload
(defun rlist-unmark-all ()
  "Unmark all registers."
  (interactive)
  (let ((inhibit-read-only t))
    (remove-text-properties (point-min) (point-max) '(display))
    (remove-text-properties (point-min) (point-max) '(marked))
    (font-lock-ensure)))

;;;###autoload
(defun rlist-read-register-for-mark (arg)
  "Return a register to mark.
If ARG is omitted or nil, this returns the register at point.
If ARG is a positive integer, this returns the next ARG registers.
If ARG is a negative integer, this returns the previous ARG registers.
If ARG is zero, and if the region is active, then this returns
the registers in the region.

In any other case, this reads a register from the minibuffer
using `register-read-with-preview'.  This includes the cases when
the ARG is not a number, or when ARG is zero but the region is
not active.

The return value is a list of registers."
  (cond
   ((null arg) (rlist-register-at-point 1))
   ((and (integerp arg) (/= arg 0)) (rlist-register-at-point arg))
   ((and (integerp arg) (= arg 0)
         (use-region-p))
    (save-restriction
      (narrow-to-region (region-beginning) (region-end))
      ;; The number of registers cannot exceed 200, at the moment.
      (save-excursion
        (goto-char (point-min))
        (rlist-register-at-point 200))))
   (t (list (register-read-with-preview "Register: ")))))

(defun rlist-register-at-point (arg)
  "Return the next/previous ARG register at point, if any."
  (cond
   ((integerp arg))
   ((error "ARG should be an integer")))
  (cond
   ((= arg 0) nil)
   ((save-excursion
      ;; First go back to a line which starts with some non-blanks.
      (forward-line 0)
      (save-match-data
        (let* ((forward-line-result 0)
               (regex rlist-register-regexp)
               (direction (cond ((> arg 0) 1) (-1)))
               (count 0)
               (arg (* arg direction))
               result stop boundary-info)
          (cond ((and (< direction 0) (bobp))
                 (setq stop t))
                ((< direction 0) (rlist-backward-register 1)))
          (while (and (not stop) (< count arg))
            (setq count (1+ count))
            (while (and (= forward-line-result 0)
                        (not (looking-at regex)))
              (setq forward-line-result (forward-line -1)))
            (cond
             ((looking-at regex)
              (setq result (cons (aref (kbd (match-string-no-properties 1)) 0) result))))
            (setq boundary-info (cond ((bobp) 1) ((eobp) -1) (0)))
            (rlist-forward-register direction)
            (cond ((or (and (= boundary-info 1) (bobp))
                       (eobp))
                   (setq stop t))))
          ;; NOTE: Normally one returns the reverse result, but since
          ;; the order does not matter for us, it is left unchanged
          ;; here.
          result))))))

;;;###autoload
(defun rlist-forward-register (&optional arg)
  "Go to the next ARG register."
  (interactive "p")
  (let* ((count 0)
         (regex rlist-register-regexp)
         (arg (prefix-numeric-value arg))
         (direction (cond ((> arg 0) 1) (-1)))
         ;; Absolute value of arg
         (arg (* arg direction)))
    (cond
     ((= arg 0))
     ((forward-line 0)                  ; 0 is non-nil
      (while (< count arg)
        (setq count (1+ count))
        (forward-line direction)
        (while (and (not (eobp)) (not (looking-at regex)))
          (forward-line direction)))))))

;;;###autoload
(defun rlist-backward-register (&optional arg)
  "Go to the previous ARG register."
  (interactive "p")
  (rlist-forward-register (- (prefix-numeric-value arg))))

;;;###autoload
(defun rlist-jump-to-register (&optional arg)
  "Jump to a register.
If ARG is nil or a non-negative integer, jump to the register at
point.

If ARG is a negative integer, reads a register from the
minibuffer using `register-read-with-preview'.

If ARG is an integer and its absolute value is 4, then pass a
non-nil arg to `jump-to-register', which causes to delete any
existing frames that the jumped-to frameset does not mention.

If ARG is not an integer nor nil, it is converted to an integer
by `prefix-numeric-value'.

This function will use another window to jump to if possible."
  (interactive "p")
  (setq arg (cond ((null arg) 0)
                  ((integerp arg) arg)
                  ((prefix-numeric-value arg))))
  (let* ((read-arg (< arg 0))
         (register (car (rlist-read-register-for-mark read-arg)))
         (jump-arg (= (abs arg) 4)))
    (cond ((one-window-p))
          ((select-window
            (next-window
             (selected-window) 'nomini 'windowframeonly))))
    (jump-to-register register jump-arg)))

(provide 'rlist)
;;; rlist.el ends here
